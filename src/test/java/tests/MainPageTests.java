package tests;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Story;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.MainPage;

@Epic("Тестирование главной страницы.")
@DisplayName("Тестирование главной страницы.")
public class MainPageTests extends TestBase {
  private String messageInConsole = "Ожидаемый и актуальный текст не равны";
  private String messageAboutVisibility = "Заголовок не отображается";
  private String expectedInfoSubscribe = "Get the latest news and offers straight to your inbox. Subscribe now.\nBy subscribing to our " +
          "newsletter you are accepting our privacy policy.\nSubscribe";
  private String expectedContactInfo = "My Store\nStreet\nPostcode City\nCountry";

  @Test
  @DisplayName("Проверить видимость элементов: логотип, региональные настройки, кнопки аккаунт и корзины.")
  @Story("Проверить видимость элементов: логотип, региональные настройки, кнопки аккаунт и корзины.")
  @Description("- Перейти на Главную страницу\n" + "- Проверить видимость элементов: логотип, региональные настройки, кнопки аккаунт и корзины.")
  public void checkVisibilityElement() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    // assert
    Assertions.assertAll(
            () -> Assertions.assertTrue(page.header.isVisibilityLogotype(), messageAboutVisibility),
            () -> Assertions.assertTrue(page.header.isVisibilityRegionalSetting(), messageAboutVisibility),
            () -> Assertions.assertTrue(page.header.isVisibilityBtnLoginPage(), messageAboutVisibility),
            () -> Assertions.assertTrue(page.header.isVisibilityCart(), messageAboutVisibility)
    );
  }

  @Test
  @DisplayName("Проверить работоспособность поиска.")
  @Story("Проверить работоспособность поиска.")
  @Description("- Перейти на Главную страницу\n" + "- Ввести в поле поиска в шапке сайта слово Red и нажать Enter\n" + "- Проверить " +
          "название заголовка страницы на соответствие тексту введенному в поле поиска.")
  public void checkSearch() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.header.setInputSearch("Red");
    // assert
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(page.isVisibilityTitlePage(), messageAboutVisibility),
            () -> Assertions.assertEquals("Red Duck", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику подменю в навигации сайта в меню Categories.")
  @Story("Проверить переход по клику подменю в навигации сайта в меню Categories.")
  @Description("- Перейти на Главную страницу\n" + "- Нажать на кнопку в подменю у пункта меню \"Categories\"\n" + "- Проверить видимость" +
          " названия страницы и ее соответствие подменю в навигации.")
  public void transitionOnClickSubmenuOfCategories() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.header.clickOnSubmenuCategoriesHeaderPanel();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("Rubber Ducks", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику подменю в навигации сайта в меню Manufacturers.")
  @Story("Проверить переход по клику подменю в навигации сайта в меню Manufacturers.")
  @Description("- Перейти на Главную страницу\n" + "- Нажать на кнопку в подменю у пункта меню \"Manufacturers\"\n" + "- Проверить видимость" +
          " названия страницы и ее соответствие подменю в навигации.")
  public void transitionOnClickSubmenuOfManufacturers() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.header.clickOnSubmenuManufacturersHeaderPanel();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("ACME Corp.", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику иконки \"дом\" в навигации сайта.")
  @Story("Проверить переход по клику иконки \"дом\" в навигации сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Нажать на кнопку в подменю у пункта меню \"Manufacturers\"\n" + "- Нажать на " +
          "иконку \"дом\" в навигации в шапке сайта\n" + "- Проверить видимость названия страницы и ее соответствие подменю в навигации.")
  public void transitionOnClickAtHome() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.header.clickOnSubmenuManufacturersHeaderPanel();
    Assertions.assertTrue(page.isVisibilityTitlePage(), messageAboutVisibility);
    page.header.clickOnIconAtHomeHeaderPanel();
    // assert
    var isVisibilitySlider = page.isVisibilityImgSlider();
    Assertions.assertTrue(isVisibilitySlider, messageAboutVisibility);
  }

  @Test
  @DisplayName("Проверить видимость слайдера на главной странице сайта.")
  @Story("Проверить видимость слайдера на главной странице сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проверить видимость слайдера на главной странице сайта.")
  public void checkVisibilitySlider() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    // assert
    var isVisibilitySlider = page.isVisibilityImgSlider();
    Assertions.assertTrue(isVisibilitySlider, messageAboutVisibility);
  }

  @Test
  @DisplayName("Проверить переход на страницу товара в первом блоке Campaign Products.")
  @Story("Проверить переход на страницу товара в первом блоке Campaign Products.")
  @Description("- Перейти на Главную страницу\n" + "- Нажать на первую карточку товара в блоке Campaign Products\n" + "- Проверить " +
          "видимость названия страницы и ее соответствие товару в карточке с главной страницы.")
  public void transitionOnClickOneCartInOneBlockProduct() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    var expectedNameProduct = page.getNameProductInOneCartInOneBlockProduct();
    page.clickOneCartInOneBlockProduct();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals(expectedNameProduct, actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход на страницу товара в первом блоке Popular Products.")
  @Story("Проверить переход на страницу товара в первом блоке Popular Products.")
  @Description("- Перейти на Главную страницу\n" + "- Нажать на первую карточку товара в блоке Popular Products\n" + "- Проверить " +
          "видимость названия страницы и ее соответствие товару в карточке с главной страницы.")
  public void transitionOnClickOneCartInTwoBlockProduct() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    var expectedNameProduct = page.getNameProductInOneCartInTwoBlockProduct();
    page.clickOneCartInTwoBlockProduct();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals(expectedNameProduct, actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход на страницу товара в первом блоке Latest Products.")
  @Story("Проверить переход на страницу товара в первом блоке Latest Products.")
  @Description("- Перейти на Главную страницу\n" + "- Нажать на первую карточку товара в блоке Latest Products\n" + "- Проверить " +
          "видимость названия страницы и ее соответствие товару в карточке с главной страницы.")
  public void transitionOnClickOneCartInThreeBlockProduct() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    var expectedNameProduct = page.getNameProductInOneCartInThreeBlockProduct();
    page.clickOneCartInThreeBlockProduct();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals(expectedNameProduct, actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить видимость блока подписки.")
  @Story("Проверить видимость блока подписки.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до блока Subscribe to our newsletter!\n" + "- Проверить " +
          "видимость блока Subscribe, текст в блоке.")
  public void checkSubscribeToNewsletter() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    // assert
    var isVisibilityTitleBlock = page.isVisibilitySubscribeNewsletter();
    var actualTitle = page.getNameSubscribeNewsletter();
    var actualInfoSubscribe = page.getInfoInSubscribeNewsletter();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitleBlock, messageAboutVisibility),
            () -> Assertions.assertEquals("Subscribe to our newsletter!", actualTitle, messageInConsole),
            () -> Assertions.assertEquals(expectedInfoSubscribe, actualInfoSubscribe, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить проверить переход по ссылки \"privacy policy\" в тексте блока Subscribe.")
  @Story("Проверить проверить переход по ссылки \"privacy policy\" в тексте блока Subscribe.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до блока Subscribe to our newsletter!\n" + "- Нажать на ссылку  " +
          "\"privacy policy\" в тексте блока Subscribe.\n" + "- Проверить что открылась страница с названием \"Privacy Policy\"")
  public void checkClickOnPrivacyPolicyInSubscribeToNewsletter() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    page.clickLinkInSubscribeNewsletter();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("Privacy Policy", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику по ссылке \"Rubber Ducks\" в подвале сайта.")
  @Story("Проверить переход по клику по ссылке \"Rubber Ducks\" в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" + "- Нажать на ссылку \"Rubber Ducks\"\n" +
          "- Проверить что открылась страница с названием \"Rubber Ducks\"")
  public void checkTransitionOnClickLinkRubberDucks() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    page.footer.clickLinkRubberDucksOnCategoriesInFooter();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("Rubber Ducks", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику по ссылке \"ACME Corp.\" в подвале сайта.")
  @Story("Проверить переход по клику по ссылке \"ACME Corp.\" в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" + "- Нажать на ссылку \"ACME Corp.\"\n" +
          "- Проверить что открылась страница с названием \"ACME Corp.\"")
  public void checkTransitionOnClickLinkACME() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    page.footer.clickLinkASMEOnManufacturersInFooter();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("ACME Corp.", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику по ссылке \"Customer Service\" в подвале сайта.")
  @Story("Проверить переход по клику по ссылке \"Customer Service\" в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" + "- Нажать на ссылку \"Customer Service\"\n" +
          "- Проверить что открылась страница с названием \"Customer Service\"")
  public void checkTransitionOnClickLinkCustomerService() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    page.footer.clickLinkCustomerServiceInAccountInFooter();
    // assert
    var isVisibilityTitle = page.isVisibilitySubTitlePage();
    var actualTitle = page.getSubTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("Customer Service", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику по ссылке \"Regional Settings\" в подвале сайта.")
  @Story("Проверить переход по клику по ссылке \"Regional Settings\" в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" + "- Нажать на ссылку \"Regional Settings\"\n" +
          "- Проверить что открылась страница с названием \"Regional Settings\"")
  public void checkTransitionOnClickLinkRegionalSettings() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    page.footer.clickLinkRegionalSettingInAccountInFooter();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("Regional Settings", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику по ссылке \"Create Account\" в подвале сайта.")
  @Story("Проверить переход по клику по ссылке \"Create Account\" в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" + "- Нажать на ссылку \"Create Account\"\n" +
          "- Проверить что открылась страница с названием \"Create Account\"")
  public void checkTransitionOnClickLinkCreateAccount() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    page.footer.clickLinkCreateAccountInAccountInFooter();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("Create Account", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику по ссылке \"Login\" в подвале сайта.")
  @Story("Проверить переход по клику по ссылке \"Login\" в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" + "- Нажать на ссылку \"Login\"\n" +
          "- Проверить что открылась страница с названием \"Login\"")
  public void checkTransitionOnClickLinkLogin() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    page.footer.clickLinkLoginInAccountInFooter();
    // assert
    var isVisibilityTitle = page.isVisibilitySubTitlePage();
    var actualTitle = page.getSubTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("Sign In", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику по ссылке \"About Us\" в подвале сайта.")
  @Story("Проверить переход по клику по ссылке \"About Us\" в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" + "- Нажать на ссылку \"About Us\"\n" +
          "- Проверить что открылась страница с названием \"About Us\"")
  public void checkTransitionOnClickLinkAboutUs() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    page.footer.clickLinkAboutUsInInformationInFooter();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("About Us", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику по ссылке \"Cookie Policy\" в подвале сайта.")
  @Story("Проверить переход по клику по ссылке \"Cookie Policy\" в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" + "- Нажать на ссылку \"Cookie Policy\"\n" +
          "- Проверить что открылась страница с названием \"Cookie Policy\"")
  public void checkTransitionOnClickLinkCookiePolicy() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    page.footer.clickLinkCookiePolicyInInformationInFooter();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("Cookie Policy", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику по ссылке \"Delivery Information\" в подвале сайта.")
  @Story("Проверить переход по клику по ссылке \"Delivery Information\" в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" + "- Нажать на ссылку \"Delivery Information\"\n" +
          "- Проверить что открылась страница с названием \"Delivery Information\"")
  public void checkTransitionOnClickLinkDeliveryInformation() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    page.footer.clickLinkDeliveryInfoInInformationInFooter();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("Delivery Information", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику по ссылке \"Privacy Policy\" в подвале сайта.")
  @Story("Проверить переход по клику по ссылке \"Privacy Policy\" в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" + "- Нажать на ссылку \"Privacy Policy\"\n" +
          "- Проверить что открылась страница с названием \"Privacy Policy\"")
  public void checkTransitionOnClickLinkPrivacyPolicy() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    page.footer.clickLinkPrivacyPoliceInInformationInFooter();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("Privacy Policy", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить переход по клику по ссылке \"Terms of Purchase\" в подвале сайта.")
  @Story("Проверить переход по клику по ссылке \"Terms of Purchase\" в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" + "- Нажать на ссылку \"Terms of Purchase\"\n" +
          "- Проверить что открылась страница с названием \"Terms of Purchase\"")
  public void checkTransitionOnClickLinkTermsPurchase() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    page.footer.clickLinkTermsPurchaseInInformationInFooter();
    // assert
    var isVisibilityTitle = page.isVisibilityTitlePage();
    var actualTitle = page.getTitlePage();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityTitle, messageAboutVisibility),
            () -> Assertions.assertEquals("Terms of Purchase", actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить контактную информацию в подвале сайта.")
  @Story("Проверить контактную информацию в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" +
          "- Проверить контактную информацию в подвале сайта.")
  public void checkContactInfo() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    // assert
    var isVisibilityItem = page.footer.isVisibilityContactInfo();
    var actualTitle = page.footer.getOurContactInfoOnFooter();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityItem, messageAboutVisibility),
            () -> Assertions.assertEquals(expectedContactInfo, actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить номер телефона в подвале сайта.")
  @Story("Проверить номер телефона в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" +
          "- Проверить номер телефона в подвале сайта.")
  public void checkPhone() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    // assert
    var isVisibilityItem = page.footer.isVisibilityPhone();
    var actualTitle = page.footer.getNumberPhoneOnFooter();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityItem, messageAboutVisibility),
            () -> Assertions.assertEquals(page.footer.phoneOnFooter, actualTitle, messageInConsole)
    );
  }

  @Test
  @DisplayName("Проверить Email в подвале сайта.")
  @Story("Проверить Email в подвале сайта.")
  @Description("- Перейти на Главную страницу\n" + "- Проскролить до футера сайта\n" +
          "- Проверить Email в подвале сайта.")
  public void checkEmail() {
    // arrange
    var page = new MainPage(driver, wait);
    page.open();
    page.closeCookies.click();
    // act
    page.scrollToFooter(page.footer.copyrightForScroll);
    // assert
    var isVisibilityItem = page.footer.isVisibilityEmail();
    var actualTitle = page.footer.getTextEmailOnFooter();
    Assertions.assertAll(
            () -> Assertions.assertTrue(isVisibilityItem, messageAboutVisibility),
            () -> Assertions.assertEquals(page.footer.emailOnFooter, actualTitle, messageInConsole)
    );
  }
}
