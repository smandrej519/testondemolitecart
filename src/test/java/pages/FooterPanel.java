package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FooterPanel {
  private WebDriver driver;
  private WebDriverWait wait;

  public static String phoneOnFooter = "+1-212-555-DUCK";
  public static String emailOnFooter = "demo@litecart.net";

  public FooterPanel(WebDriver driver, WebDriverWait wait) {
    this.driver = driver;
    this.wait = wait;
    PageFactory.initElements(driver, this);
  }

  @FindBy(css = "#scroll-up")
  private WebElement scrollUp;

  @FindBy(css = "#footer .categories ul a")
  private WebElement oneItemOnCategoriesInFooter;

  @FindBy(css = "#footer .manufacturers ul a")
  private WebElement oneItemOnManufacturersInFooter;

  @FindBy(css = "#footer .account li:nth-of-type(1) a")
  private WebElement linkCustomerServiceInAccountInFooter;

  @FindBy(css = "#footer .account li:nth-of-type(2) a")
  private WebElement linkRegionalSettingInAccountInFooter;

  @FindBy(css = "#footer .account li:nth-of-type(3) a")
  private WebElement linkCreateAccountInAccountInFooter;

  @FindBy(css = "#footer .account li:nth-of-type(4) a")
  private WebElement linkLoginInAccountInFooter;

  @FindBy(css = "#footer .information li:nth-of-type(1) a")
  private WebElement linkAboutUsInInformationInFooter;

  @FindBy(css = "#footer .information li:nth-of-type(2) a")
  private WebElement linkCookiePolicyInInformationInFooter;

  @FindBy(css = "#footer .information li:nth-of-type(3) a")
  private WebElement linkDeliveryInfoInInformationInFooter;

  @FindBy(css = "#footer .information li:nth-of-type(4) a")
  private WebElement linkPrivacyPoliceInInformationInFooter;

  @FindBy(css = "#footer .information li:nth-of-type(5) a")
  private WebElement linkTermsPurchaseInInformationInFooter;

  @FindBy(css = "#footer .contact li:nth-of-type(1)")
  private WebElement fieldOurContactInFooter;

  @FindBy(css = "#footer .contact li:nth-of-type(2) a")
  private WebElement fieldPhoneContactInFooter;

  @FindBy(css = "#footer .contact li:nth-of-type(3) a")
  private WebElement fieldEmailContactInFooter;

  @FindBy(css = "#copyright")
  public WebElement copyrightForScroll;

  public Boolean isVisibilityContactInfo() {
    wait.until(ExpectedConditions.visibilityOf(fieldOurContactInFooter));
    return fieldOurContactInFooter.isDisplayed();
  }

  @Step("Проверка контактной информация в футоре")
  public String getOurContactInfoOnFooter() {
    wait.until(ExpectedConditions.visibilityOf(fieldOurContactInFooter));
    return fieldOurContactInFooter.getText();
  }

  public Boolean isVisibilityPhone() {
    wait.until(ExpectedConditions.visibilityOf(fieldPhoneContactInFooter));
    return fieldPhoneContactInFooter.isDisplayed();
  }

  @Step("Проверка Номера телефона в футоре")
  public String getNumberPhoneOnFooter() {
    wait.until(ExpectedConditions.visibilityOf(fieldPhoneContactInFooter));
    return fieldPhoneContactInFooter.getText();
  }

  public Boolean isVisibilityEmail() {
    wait.until(ExpectedConditions.visibilityOf(fieldEmailContactInFooter));
    return fieldEmailContactInFooter.isDisplayed();
  }

  @Step("Проверка Email в футоре")
  public String getTextEmailOnFooter() {
    wait.until(ExpectedConditions.visibilityOf(fieldEmailContactInFooter));
    return fieldEmailContactInFooter.getText();
  }

  @Step("Клик по ссылки подменю в блоке Categories в подвале сайта.")
  public void clickLinkRubberDucksOnCategoriesInFooter() {
    oneItemOnCategoriesInFooter.click();
  }

  @Step("Клик по ссылки подменю в блоке Categories в подвале сайта.")
  public void clickLinkASMEOnManufacturersInFooter() {
    oneItemOnManufacturersInFooter.click();
  }

  @Step("Клик по ссылки Customer Service в блоке Account в подвале сайта.")
  public void clickLinkCustomerServiceInAccountInFooter() {
    linkCustomerServiceInAccountInFooter.click();
  }

  @Step("Клик по ссылки Regional Setting в блоке Account в подвале сайта.")
  public void clickLinkRegionalSettingInAccountInFooter() {
    linkRegionalSettingInAccountInFooter.click();
  }

  @Step("Клик по ссылки Create Account в блоке Account в подвале сайта.")
  public void clickLinkCreateAccountInAccountInFooter() {
    linkCreateAccountInAccountInFooter.click();
  }

  @Step("Клик по ссылки Login в блоке Account в подвале сайта.")
  public void clickLinkLoginInAccountInFooter() {
    linkLoginInAccountInFooter.click();
  }

  @Step("Клик по ссылки About Us в блоке Account в подвале сайта.")
  public void clickLinkAboutUsInInformationInFooter() {
    linkAboutUsInInformationInFooter.click();
  }

  @Step("Клик по ссылки Cookie Policy в блоке Account в подвале сайта.")
  public void clickLinkCookiePolicyInInformationInFooter() {
    linkCookiePolicyInInformationInFooter.click();
  }

  @Step("Клик по ссылки Delivery Information в блоке Account в подвале сайта.")
  public void clickLinkDeliveryInfoInInformationInFooter() {
    linkDeliveryInfoInInformationInFooter.click();
  }

  @Step("Клик по ссылки Privacy Police в блоке Account в подвале сайта.")
  public void clickLinkPrivacyPoliceInInformationInFooter() {
    linkPrivacyPoliceInInformationInFooter.click();
  }

  @Step("Клик по ссылки Terms of Purchase в блоке Account в подвале сайта.")
  public void clickLinkTermsPurchaseInInformationInFooter() {
    linkTermsPurchaseInInformationInFooter.click();
  }
}
