package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
  protected WebDriver driver;
  protected WebDriverWait wait;
  public HeaderPanel header;
  public FooterPanel footer;

  public BasePage(WebDriver driver, WebDriverWait wait, String subUrl) {
    this.driver = driver;
    this.wait = wait;
    this.subUrl = subUrl;
    header = new HeaderPanel(driver, wait);
    footer = new FooterPanel(driver, wait);
    PageFactory.initElements(driver, this);
  }

  public BasePage(WebDriver driver, WebDriverWait wait) {
    this(driver, wait, "");
  }

  @Step("Открыть страницу")
  public void open() {
    driver.navigate().to(getPageUrl());
  }

  private String getPageUrl() {
    return url + subUrl;
  }

  private String url = "https://demo.litecart.net/";
  private String subUrl;

  public String parseText(String text) {
    var str = text.trim();
    var str2 = str.toLowerCase();
    var str3 = str2.replaceAll("[^A-ZА-Яa-zа-я0-9\\s]", "");
    return str3;
  }

  @FindBy(css = "button[name=\"decline_cookies\"]")
  public WebElement closeCookies;

  @FindBy(css = "h1")
  private WebElement titlePage;

  @FindBy(css = "h2")
  private WebElement subTitlePage;

  public Boolean isVisibilityTitlePage() {
    wait.until(ExpectedConditions.visibilityOf(titlePage));
    return titlePage.isDisplayed();
  }

  @Step("Проверка Название страницы")
  public String getTitlePage() {
    return titlePage.getText();
  }

  public Boolean isVisibilitySubTitlePage() {
    wait.until(ExpectedConditions.visibilityOf(subTitlePage));
    return subTitlePage.isDisplayed();
  }

  @Step("Проверка Название страницы")
  public String getSubTitlePage() {
    return subTitlePage.getText();
  }

  public Boolean containsText(String expected, String actual) {
    return parseText(actual).contains(parseText(expected));
  }

  @Step("Скролл страницы к футору")
  public void scrollToFooter(WebElement webElement) {
    new Actions(driver)
        .moveToElement(webElement)
        .perform();
    wait.until(ExpectedConditions.visibilityOf(webElement));
  }
}
