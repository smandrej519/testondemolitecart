package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage extends BasePage {
  public MainPage(WebDriver driver, WebDriverWait wait) {
    super(driver, wait, "");
    PageFactory.initElements(driver, this);
  }

  @FindBy(css = "#box-slides .item img")
  private WebElement imgSlider;

  @FindBy(css = "#box-campaign-products .product:nth-of-type(1)")
  private WebElement oneCartInOneBlockProduct;

  @FindBy(css = "#box-campaign-products .product:nth-of-type(1) h4.name")
  private WebElement nameProductInOneCartInOneBlockProduct;

  @FindBy(css = "#box-popular-products .product:nth-of-type(1)")
  private WebElement oneCartInTwoBlockProduct;

  @FindBy(css = "#box-popular-products .product:nth-of-type(1) h4.name")
  private WebElement nameProductInOneCartInTwoBlockProduct;

  @FindBy(css = "#box-latest-products .product:nth-of-type(1)")
  private WebElement oneCartInThreeBlockProduct;

  @FindBy(css = "#box-latest-products .product:nth-of-type(1) h4.name")
  private WebElement nameProductInOneCartInThreeBlockProduct;

  @FindBy(css = "#box-newsletter-subscribe h2.card-title")
  private WebElement subscribeNewsletter;

  @FindBy(css = "#box-newsletter-subscribe form[name=\"newsletter_subscribe_form\"]")
  private WebElement infoInSubscribeNewsletter;

  @FindBy(css = "#box-newsletter-subscribe form[name=\"newsletter_subscribe_form\"] a")
  private WebElement linkInSubscribeNewsletter;

  public Boolean isVisibilityImgSlider() {
    wait.until(ExpectedConditions.visibilityOf(imgSlider));
    return imgSlider.isDisplayed();
  }

  @Step("Клик по первой карточке в первом блоке Campaign Products.")
  public void clickOneCartInOneBlockProduct() {
    oneCartInOneBlockProduct.click();
  }

  @Step("Проверка Название товара в первом блоке Campaign Products:")
  public String getNameProductInOneCartInOneBlockProduct() {
    wait.until(ExpectedConditions.visibilityOf(nameProductInOneCartInOneBlockProduct));
    return nameProductInOneCartInOneBlockProduct.getText();
  }

  @Step("Клик по первой карточке в первом блоке Popular Products.")
  public void clickOneCartInTwoBlockProduct() {
    oneCartInTwoBlockProduct.click();
  }

  @Step("Проверка Название товара в первом блоке Popular Products:")
  public String getNameProductInOneCartInTwoBlockProduct() {
    wait.until(ExpectedConditions.visibilityOf(nameProductInOneCartInTwoBlockProduct));
    return nameProductInOneCartInTwoBlockProduct.getText();
  }

  @Step("Клик по первой карточке в первом блоке Latest Products.")
  public void clickOneCartInThreeBlockProduct() {
    oneCartInThreeBlockProduct.click();
  }

  @Step("Клик по ссылки в тексте Subscribe to our newsletter!.")
  public void clickLinkInSubscribeNewsletter() {
    linkInSubscribeNewsletter.click();
  }

  @Step("Проверка Название товара в первом блоке Latest Products:")
  public String getNameProductInOneCartInThreeBlockProduct() {
    wait.until(ExpectedConditions.visibilityOf(nameProductInOneCartInThreeBlockProduct));
    return nameProductInOneCartInThreeBlockProduct.getText();
  }

  public Boolean isVisibilitySubscribeNewsletter() {
    wait.until(ExpectedConditions.visibilityOf(subscribeNewsletter));
    return subscribeNewsletter.isDisplayed();
  }

  @Step("Проверка Название блока подписки")
  public String getNameSubscribeNewsletter() {
    wait.until(ExpectedConditions.visibilityOf(subscribeNewsletter));
    return subscribeNewsletter.getText();
  }

  @Step("Проверка Текста в блоке подписки")
  public String getInfoInSubscribeNewsletter() {
    wait.until(ExpectedConditions.visibilityOf(infoInSubscribeNewsletter));
    return infoInSubscribeNewsletter.getText();
  }
}
