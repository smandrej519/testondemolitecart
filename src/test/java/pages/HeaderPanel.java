package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HeaderPanel {
  private WebDriver driver;
  private WebDriverWait wait;

  public HeaderPanel(WebDriver driver, WebDriverWait wait) {
    this.driver = driver;
    this.wait = wait;
    PageFactory.initElements(driver, this);
  }

  @FindBy(css = "a.logotype img")
  private WebElement logotype;

  @FindBy(css = ".navbar-search")
  private WebElement search;

  @FindBy(css = "input[data-type=\"search\"]")
  private WebElement inputSearch;

  @FindBy(css = "a.regional-setting")
  private WebElement regionalSetting;

  @FindBy(css = "a.account")
  private WebElement btnLoginPage;

  @FindBy(css = "#cart")
  private WebElement cart;

  @FindBy(css = "#offcanvas .nav-item a")
  private WebElement btnAtHome;

  @FindBy(css = "#offcanvas .categories a")
  private WebElement btnCategories;

  @FindBy(css = "#offcanvas .categories .dropdown-menu a")
  private WebElement btnSubmenuCategories;

  @FindBy(css = "#offcanvas .manufacturers a")
  private WebElement btnManufacturers;

  @FindBy(css = "#offcanvas .manufacturers .dropdown-menu a")
  private WebElement btnSubmenuManufacturers;

  @FindBy(css = "#offcanvas .customer-service a")
  private WebElement btnCustomerService;

  @FindBy(css = "#offcanvas .account > a")
  private WebElement dropdownAccount;

  @FindBy(css = "form[name=\"login_form\"]")
  private WebElement formLogin;

  @FindBy(css = "#offcanvas .text-center:nth-of-type(2) a")
  private WebElement createAccount;

  @FindBy(css = "#offcanvas .text-center:nth-of-type(3) a")
  private WebElement lostPassword;

  public Boolean isVisibilityLogotype() {
    wait.until(ExpectedConditions.visibilityOf(logotype));
    return logotype.isDisplayed();
  }

  public Boolean isVisibilityRegionalSetting() {
    wait.until(ExpectedConditions.visibilityOf(regionalSetting));
    return regionalSetting.isDisplayed();
  }

  public Boolean isVisibilityBtnLoginPage() {
    wait.until(ExpectedConditions.visibilityOf(btnLoginPage));
    return btnLoginPage.isDisplayed();
  }

  public Boolean isVisibilityCart() {
    wait.until(ExpectedConditions.visibilityOf(cart));
    return cart.isDisplayed();
  }

  @Step("Клик по кнопке подменю во вкладке Categories.")
  public void clickOnSubmenuCategoriesHeaderPanel() {
    new Actions(driver)
            .click(btnCategories)
            .click(btnSubmenuCategories)
            .perform();
  }

  @Step("Клик по кнопке подменю во вкладке Manufacturers.")
  public void clickOnSubmenuManufacturersHeaderPanel() {
    new Actions(driver)
            .click(btnManufacturers)
            .click(btnSubmenuManufacturers)
            .perform();
  }

  @Step("Клик по иконки \"дом\" в навигации сайта.")
  public void clickOnIconAtHomeHeaderPanel() {
    btnAtHome.click();
  }

  @Step("Ввод текста в поле поиска {str}.")
  public void setInputSearch(String str) {
    inputSearch.sendKeys(str + Keys.ENTER);
  }
}
